const testUrl = '/test/basic-checkbox-demo.html'
const msg = 'Success - Check box is checked'

describe('Checkbox tests', function(){

    beforeEach(function () {
        cy.visit(testUrl)
     })

     it('Check if message is not diplayed when checkbox is not ticked', function(){
        cy.get('#txtAge').should('not.be.visible')
     })

    it('Check if message is displayed after checkbox tick', function(){
        cy.get('#isAgeSelected').click()
        cy.contains(msg)
    })

    it('Check if one checkbox changes state of "Check All" button', function() {
        cy.get(':nth-child(3) > label').click()
        cy.get('#check1').should('have.value', 'Check All')
    })
        
    it('Check if checking all checkbox changes button to "Uncheck All"', function(){
        cy.get(':nth-child(3) > label').click()
        cy.get(':nth-child(4) > label').click()
        cy.get(':nth-child(5) > label').click()
        cy.get(':nth-child(6) > label').click()
        cy.get('#check1').should('have.value', 'Uncheck All')
    })

    it('Check if click at "Check All" checks all checkboxes', function(){
        cy.get('#check1').click()
        cy.get(':nth-child(3) > label > .cb1-element').should('be.checked')
        cy.get(':nth-child(4) > label > .cb1-element').should('be.checked')
        cy.get(':nth-child(5) > label > .cb1-element').should('be.checked')
        cy.get(':nth-child(6) > label > .cb1-element').should('be.checked')
    })
})