const testUrl = '/test/basic-first-form-demo.html'

describe('Input forms tests', function(){

    beforeEach(function () {
        cy.visit(testUrl)
     })

    it('Check if message is displayed in "Single Input Field"', function(){
        cy.get('.form-group > #user-message').type('test')
        cy.get('#get-input > .btn').click()
        cy.contains('Your Message: test')
    })

    it('Check empty values for "Two Input Fields"', function(){
        cy.get('#gettotal > .btn').click()
        cy.contains('Total a + b = NaN')
    })

    it('Check if non-numeric value returns NaN', function(){
        cy.get('#sum1').type('string')
        cy.get('#sum2').type('2')
        cy.get('#gettotal > .btn').click()
        cy.contains('Total a + b = NaN')
    })

    it('Check if two numbers are added correctly', function(){
        cy.get('#sum1').type('28')
        cy.get('#sum2').type('6362')
        cy.get('#gettotal > .btn').click()
        cy.contains('Total a + b = 6390')
    })
})