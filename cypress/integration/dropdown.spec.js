const testUrl = '/test/basic-select-dropdown-demo.html'

describe('Checkbox tests', function(){

    beforeEach(function () {
        cy.visit(testUrl)
     })

     it('Check if default state of dropdown is correct in "Select List Demo"', function(){
        cy.get('#select-demo').select('Friday').should('have.value', 'Friday')
        cy.contains('Day selected :- Friday')
     })

     it('Check if correct values are returned in "Multi Select List Demo"', function(){
        cy.get(':nth-child(5) > .panel-body').trigger('keydown', { keyCode: 17 })
        cy.get('[value="Florida"]').click()
        cy.get('[value="Pennsylvania"]').click()
        cy.get(':nth-child(5) > .panel-body').trigger('keyup', { keyCode: 17 })
        cy.get('#printAll').click()
        cy.contains('Options selected are : Pennsylvania')
     })
})