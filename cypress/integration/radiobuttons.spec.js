const testUrl = '/test/basic-radiobutton-demo.html'

describe('Checkbox tests', function(){

    beforeEach(function () {
        cy.visit(testUrl)
     })

     it('Check if none of radio is checked in "Radio Button Demo"', function(){
        cy.get('#buttoncheck').click()
        cy.contains('Radio button is Not checked')
     })

     it('Check if correct sex value is returned in "Radio Button Demo"', function(){
        cy.get('.panel-body > :nth-child(3) > input').click()
        cy.get('#buttoncheck').click()
        cy.contains("Radio button 'Female' is checked")
     })

     it('Check values if only one radio is checked in "Group Radio Buttons Demo"', function(){
        cy.get(':nth-child(4) > input').click()
        cy.get('.panel-body > .btn').click()
        cy.contains('Sex : Age group: 15 - 50')
     })
    
     it('Check if both values are visible when both radios are checked in "Group Radio Button Demo"', function(){
        cy.get('.panel-body > :nth-child(2) > :nth-child(2) > input').click()
        cy.get(':nth-child(4) > input').click()
        cy.get('.panel-body > .btn').click()
        cy.contains('Sex : Male Age group: 15 - 50')
     })
})